#!/bin/bash

rm -rf ../build-debug
mkdir ../build-debug
cd ../build-debug

cmake .. -DCMAKE_BUILD_TYPE=Debug
make

./tebrafile

mkdir results
cd results
lcov -d .. -c -o tebrafile.info

genhtml tebrafile.info
xdg-open index.html

echo "Done"
