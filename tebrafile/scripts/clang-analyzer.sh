#!/bin/bash

rm -rf ../build-debug
mkdir ../build-debug
cd ../build-debug

cmake .. -DCMAKE_BUILD_TYPE=Deb -DCMAKE_CXX_COMPILER=/usr/lib/llvm-10/libexec/c++-analyzer
scan-build-10 --use-analyzer=/usr/bin/clang++-10 make
scan-view-10 --no-browser /tmp/scan-build-2021-04-25-135507-14690-1

echo "Done"
