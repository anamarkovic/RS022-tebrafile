#ifndef SEARCH_H
#define SEARCH_H

#include "listFiles.h"

#include <QDir>
#include <QFtp>
#include <QHash>
#include <QHeaderView>
#include <QSharedPointer>
#include <QThread>
#include <QTreeWidget>
#include <iostream>

class Search : public QThread
{
    Q_OBJECT
public:
    Search(QSharedPointer<ListFiles> treeWidget,
           const QSharedPointer<QRegularExpression> &filename,
           QString path,
           QSharedPointer<QFtp> client,
           QObject *parent = nullptr);
    ~Search()
    {}
    void stopSearch();
    void addToList(const QUrlInfo &file);
    bool isOngoing() const;
    int numOfFoundItems() const;

private slots:
    void folderFinished(bool error);

signals:
    void searchFinished();

protected:
    void run() override;

private:
    QSharedPointer<ListFiles> _treeWidget;
    QSharedPointer<QRegularExpression> _filename;
    QString _path;
    QSharedPointer<QFtp> _client;
    QVector<QString> _folders;
    bool _stop = false;
    bool _ongoing = false;
};

#endif // SEARCH_H
