#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "inputDialog.h"
#include "listFiles.h"
#include "loader.h"
#include "search.h"
#include "serverconnection.h"

#include <iostream>

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QFtp>
#include <QHash>
#include <QHeaderView>
#include <QListWidgetItem>
#include <QMainWindow>
#include <QMessageBox>
#include <QMutex>
#include <QStandardPaths>
#include <QString>
#include <QThread>
#include <QTreeWidget>
#include <QUrl>
#include <QIcon>

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void getFileList();
    static QMutex uploadMutex;
    static QMutex downloadMutex;

    QSharedPointer<QFtp> &getClient();
    QSharedPointer<Logger> getLogger();
    Ui::MainWindow *getUI();
    ServerConnection *getConnection();
public slots:
    void initTreeWidget();

private slots:
    void on_connectButton_clicked();
    void on_disconnectButton_clicked();
    void on_openButton_clicked();
    void on_uploadButton_clicked();
    void on_downloadButton_clicked();
    void on_treeWidget_clicked();
    void uploadProgressBarSlot(int id, qint64 done, qint64 total);
    void downloadProgressBarSlot(int id, qint64 done, qint64 total);
    void pwdHandler(int replyCode, const QString &detail);

    void uploadErrorHandler();
    void downloadErrorHandler();

    void on_downloadCancel_clicked();

    void on_searchButton_clicked();

private:
    Ui::MainWindow *ui;
    ServerConnection *serverConn = nullptr;

    QVector<Loader *> loaders;

    QSharedPointer<Logger> _logger;
    QString path;

    ListFiles *fileList = nullptr;

    QSharedPointer<QFtp> client;
};
#endif // MAINWINDOW_H
